#ifndef __UART_MANAGER__
#define __UART_MANAGER__

#ifdef __cplusplus
extern "C" {
#endif

/************constrant defination**************/
#define SIMPLE_TASK	(0xF1)

//tasks handle function
typedef void (*uart_handle)(const char *);

extern int uart_manager_send(const char *, unsigned int );
extern void uart_pthread_wait_exit();
extern int uart_open(const char *);
extern uart_close();

#ifdef __cplusplus
}
#endif
#endif
