#include <stdio.h>
#include <uart_manager.h>

#define UART_PORT "/dev/ttySAC3"

int main(int argc, char *argv[])
{
	uart_open(UART_PORT);
	uart_pthread_wait_exit();
	uart_close();
	return 0;
}
