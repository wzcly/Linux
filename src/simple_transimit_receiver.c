#include <stdio.h>
#include <string.h>
#include <uart_manager.h>

void simple_task_handle(const char *recv)
{
	if(NULL == recv)
	{
		printf("recv == NULL\n");
		return;
	}
	printf("send data+++\n");
	uart_manager_send(recv, strlen(recv));
	printf("send data---\n");
}

